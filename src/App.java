import models.Circle;
import models.Rectangle;
import models.Square;

public class App {
    public static void main(String[] args) throws Exception {
        Rectangle rectangle = new Rectangle("green", false, 20, 10);

        Circle circle = new Circle("blue", true, 15);

        Square square = new Square("yellow", false, 12);
        
        System.out.println("Rectangle");
        System.out.println("Dien tich" + rectangle.getArea());
        System.out.println("Chi vi" + rectangle.getPerimeter());
        System.out.println("Circle");
        System.out.println("Dien tich" + circle.getArea());
        System.out.println("Chi vi" + circle.getPerimeter());
        System.out.println("Square");
        System.out.println("Dien tich" + square.getArea());
        System.out.println("Chi vi" + square.getPerimeter());

        System.out.println(rectangle);
        System.out.println(circle);
        System.out.println(square);
    }
}
